﻿using System.Threading.Tasks;
using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    public class TestCommandHandler : ICommandHandler<TestCommand>
    {
        public TestCommand CommandHandled { get; private set; }

        public async Task Handle(TestCommand command)
        {
            CommandHandled = command;                      
        }

        
    }
}