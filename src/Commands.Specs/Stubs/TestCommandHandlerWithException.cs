﻿using System;
using System.Threading.Tasks;
using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    public class TestCommandHandlerWithException : ICommandHandler<TestCommand>
    {
        readonly Exception _exceptionToThrow;

        public TestCommandHandlerWithException(Exception exceptionToThrow)
        {
            _exceptionToThrow = exceptionToThrow;
        }

        public Task Handle(TestCommand command)
        {
            throw _exceptionToThrow;
        }
    }
}