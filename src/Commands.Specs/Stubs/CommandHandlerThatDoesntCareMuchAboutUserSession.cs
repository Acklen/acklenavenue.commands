﻿using System.Threading.Tasks;
using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    public class CommandHandlerThatDoesntCareMuchAboutUserSession : IHandler<TestCommand>
    {
        public async Task Handle(TestCommand command)
        {
            Ran = true;
        }

        public bool Ran { get; private set; }
    }
}