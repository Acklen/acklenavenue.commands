using System;
using System.Threading.Tasks;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    internal class ValidatorWithError : ICommandValidator<TestCommand>
    {
        public Task Validate(TestCommand command)
        {
            throw new Exception("Validation error");
        }
    }
}