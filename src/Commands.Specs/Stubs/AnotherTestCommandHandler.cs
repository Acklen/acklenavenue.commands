﻿using System.Threading.Tasks;
using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    public class AnotherTestCommandHandler : ICommandHandler<AnotherTestCommand>
    {
        public AnotherTestCommand CommandHandled { get; private set; }

        public async Task Handle(AnotherTestCommand command)
        {
            CommandHandled = command;
        }

    }
}