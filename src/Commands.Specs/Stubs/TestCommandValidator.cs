using System.Threading.Tasks;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    public class TestCommandValidator : ICommandValidator<TestCommand>, ICommandValidator<TestCommand2>
    {
        public TestCommand CommandValidated { get; private set; }
        public TestCommand2 Command2Validated { get; private set; }
        
        public async Task Validate(TestCommand command)
        {
            CommandValidated = command;
        }

        public async Task Validate(TestCommand2 command)
        {
            Command2Validated = command;
        }
    }
}