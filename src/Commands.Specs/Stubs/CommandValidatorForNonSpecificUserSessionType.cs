using System.Threading.Tasks;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    public class CommandValidatorForNonSpecificUserSessionType : ICommandValidator<TestCommand>
    {
        public async Task Validate(TestCommand command)
        {
            this.Ran = true;
        }
        
        public bool Ran { private set; get; }
    }
}