﻿using System.Collections.Generic;
using AcklenAvenue.Commands.Specs.Stubs;
using AcklenAvenue.Dispatch;
using Machine.Specifications;

namespace AcklenAvenue.Commands.Specs.Testing
{
    public class when_dispatching_a_command
    {
        static WorkerCommandDispatcher _dispatcher;
        static TestCommandHandler _testCommandHandler;
        static readonly TestCommand TestCommand = new TestCommand();

        Establish context =
            () =>
            {
                _testCommandHandler = new TestCommandHandler();
                var commandHandlers = new List<ICommandHandler>
                {
                    _testCommandHandler,
                    new AnotherTestCommandHandler()
                };

                _dispatcher = new WorkerCommandDispatcher(commandHandlers);
            };

        Because of =
            () => _dispatcher.Dispatch(TestCommand).Await();

        It should_dispatch_the_expected_command =
            () => _testCommandHandler.CommandHandled.ShouldEqual(TestCommand);

    }
}