﻿using System;
using System.Collections.Generic;
using AcklenAvenue.Commands.Specs.Stubs;
using AcklenAvenue.Dispatch;
using Machine.Specifications;

namespace AcklenAvenue.Commands.Specs.Testing
{
    public class when_dispatching_a_command_with_an_exception
    {
        static WorkerCommandDispatcher _dispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;
        static Exception _exceptionToThrow;

        Establish context =
            () =>
            {
                _exceptionToThrow = new Exception("Test");
                var commandHandlers = new List<ICommandHandler>
                                      {
                                          new TestCommandHandlerWithException(_exceptionToThrow)
                                      };
                _dispatcher = new WorkerCommandDispatcher(commandHandlers);
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.ShouldEqual(_exceptionToThrow);
    }
}