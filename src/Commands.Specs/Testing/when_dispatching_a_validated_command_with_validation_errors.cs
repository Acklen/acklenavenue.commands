using System;
using System.Collections.Generic;
using AcklenAvenue.Commands.Specs.Stubs;
using AcklenAvenue.Dispatch;
using Machine.Specifications;

namespace AcklenAvenue.Commands.Specs.Testing
{
    public class when_dispatching_a_validated_command_with_validation_errors
    {
        static ValidatedCommandDispatcher _dispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;

        Establish context =
            () =>
            {
                var commandHandlers = new List<ICommandHandler>
                {
                    new TestCommandHandler()
                };
                var commandValidators = new List<ICommandValidator>()
                {
                    new ValidatorWithError()
                };

                _dispatcher = new ValidatedCommandDispatcher(new WorkerCommandDispatcher(commandHandlers), commandValidators);
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.Message.ShouldEqual("Validation error");
    }
}