using System.Threading.Tasks;

namespace AcklenAvenue.Commands
{
    public class QueuedCommandDispatcher : ICommandDispatcher
    {
        readonly ICommandQueuePublisher _publisher;

        public QueuedCommandDispatcher(ICommandQueuePublisher publisher)
        {
            _publisher = publisher;
        }

        public async Task Dispatch(object command)
        {
            await _publisher.Publish(command);
        }
    }
}