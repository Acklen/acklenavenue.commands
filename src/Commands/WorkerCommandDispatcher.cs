using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands
{
    public class WorkerCommandDispatcher : ICommandDispatcher
    {
        readonly Dispatcher _dispatcher;

        public WorkerCommandDispatcher(IEnumerable<ICommandHandler> commandHandlers, IDispatcherLogger logger = null)
        {
            _dispatcher = new Dispatcher(new DefaultHandlerMatcher(commandHandlers.Select(x=> (IHandler)x)),
                new CommandDispatcherConfig(logger ?? new QuietDispatcherLogger())
            );
        }

        public async Task Dispatch(object command)
        {
            await _dispatcher.Dispatch(command);
        }
    }
}