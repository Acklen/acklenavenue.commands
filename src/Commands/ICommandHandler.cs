using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands
{
    public interface ICommandHandler
    {
    }

    public interface ICommandHandler<in TEvent> : IHandler<TEvent>, ICommandHandler
    {
    }
}