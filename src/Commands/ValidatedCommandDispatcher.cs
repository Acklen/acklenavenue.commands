using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands
{
    public class ValidatedCommandDispatcher : ICommandDispatcher
    {
        readonly IDispatcher _decoratedDispatcher;
        readonly IEnumerable<ICommandValidator> _commandValidators;

        public ValidatedCommandDispatcher(IDispatcher decoratedDispatcher, IEnumerable<ICommandValidator> commandValidators)
        {
            _decoratedDispatcher = decoratedDispatcher;
            _commandValidators = commandValidators;
        }
        
        public async Task Dispatch(object command)
        {
            await Validate(command);
            await _decoratedDispatcher.Dispatch(command);
        }

        async Task Validate(object command)
        {
            var matchingValidators = _commandValidators.Where(HasAnInterfaceThatMatchesGenericArgs(command));
            foreach (var matchingValidator in matchingValidators)
                await InvokeMethod("Validate", matchingValidator, command).ConfigureAwait(false);
        }

        static Func<object, bool> HasAnInterfaceThatMatchesGenericArgs(object command)
        {
            return handler =>
            {
                var interfaces = handler.GetType()
                    .GetInterfaces();

                return interfaces
                    .Any(
                        i => i.GenericTypeArguments.Length == 1 &&
                             i.GenericTypeArguments[0] == command.GetType());
            };
        }

        async Task InvokeMethod(string methodName, object invokableObject, params object[] methodArgs)
        {
            var methodArgTypes = methodArgs.Select(x => x.GetType()).ToArray();
            try
            {
                var handlerMethod = invokableObject.GetType().GetMethod(methodName, methodArgTypes);
                await ((Task) handlerMethod.Invoke(invokableObject, methodArgs)).ConfigureAwait(false);
            }
            catch (TargetInvocationException ex)
            {
                throw ex.InnerException;
            }
            catch (AggregateException ex)
            {
                throw ex.InnerException;
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
        }
    }
}