using System.Threading.Tasks;

namespace AcklenAvenue.Commands
{
    public interface ICommandQueuePublisher
    {
        Task Publish(object command);
    }
}