using System.Threading.Tasks;

namespace AcklenAvenue.Commands
{
    public interface ICommandValidator
    {
    }

    public interface ICommandValidator<in TCommand> : ICommandValidator
    {
        Task Validate(TCommand command);
    }
}