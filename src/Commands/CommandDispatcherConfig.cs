using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands
{
    public class CommandDispatcherConfig : IDispatcherConfig
    {
        public CommandDispatcherConfig(IDispatcherLogger logger)
        {
            Logger = logger;
        }

        public IDispatcherLogger Logger { get; }
        public int MaximumHandlersPerDispatch => 1;
        public int MinimumHandlersPerDispatch => 1;
    }
}